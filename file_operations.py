"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    with open(path, 'r', encoding = 'utf-8') as f:
        f.read()
    f.close

def write_to_file(path, s):
    with open(path, 'w', encoding = 'utf-8') as f:
        f.write(s)
    f.close()

def append_to_file(path, s):
    with open(path, 'a', encoding = "utf-8") as f:
        f.write(s)

import csv
def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    naturalNumbersAndSquares = [{'natural_num': num, 'square': num*num} for num in range(n+1)]
    with open(file_path, 'w', encoding = 'utf-8') as csvFile:
        fields = ['natural_num', 'square']
        writer = csv.DictWriter(csvFile, fieldnames=fields)
        writer.writeheader()
        writer.writerows(naturalNumbersAndSquares)
    csvFile.close()

numbers_and_squares(5, 'naturals_and_squares.csv')
