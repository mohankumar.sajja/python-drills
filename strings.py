def last_3_characters(x):
    return x[len(x)-3:]
print(last_3_characters('Mohan Kumar Sajja'))


def first_10_characters(x):
    return x[0:10]
print(first_10_characters('Mohan Kumar Sajja'))


def chars_4_through_10(x):
    return x[3:10]
print(chars_4_through_10('Mohan Kumar Sajja'))


def str_length(x):
    return len(x)


def words(x):
    return len(x.split(' '))
print(words('Mohan Kumar Sajja'))


def capitalize(x):
    return x.capitalize()

print(capitalize('mohan Kumar Sajja'))


def to_uppercase(x):
    return x.upper()
print(to_uppercase('mohan kumar sajja'))
