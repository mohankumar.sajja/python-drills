def sum_items_in_list(x):
    return sum(x)

print(sum_items_in_list([1, 2, 3]))

def list_length(x):
    return len(x)


def last_three_items(x):
    #return list(reversed(x[-1:-4:-1]))
    return x[len(x)-3:]

print(last_three_items([1, 2, 3, 4, 5]))


def first_three_items(x):
    return x[0:3]

print(first_three_items([1,2,3,4,5]))


def sort_list(x):
    # x.sort()
    # return x
    return sorted(x)

print(sort_list([3, 6, 2, 4, 1]))


def append_item(x, item):
    x.append(item)
    return x

print(append_item([1, 2, 3], 4))


def remove_last_item(x):
    return x.pop()
print(remove_last_item([1,2,3,4,5]))


def count_occurrences(x, item):
    return x.count(item)

print(count_occurrences([1, 2, 2 ,2, 3], 2))

def is_item_present_in_list(x, item):
    return item in x
print(is_item_present_in_list([1, 3, 5, 6], 3))


def append_all_items_of_y_to_x(x, y):
    """
    x and y are lists
    """
    x += y
    return x
print(append_all_items_of_y_to_x([1, 2, 3], [4, 5]))

import copy
def list_copy(x):
    """
    Create a shallow copy of x
    """
    y = copy.copy(x)
    return y
print(list_copy([1, 2, [4, 5, 6]]))