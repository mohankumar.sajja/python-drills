def integers_from_start_to_end_using_range(start, end, step):
    print(list(range(start, end+1, step)))
    
integers_from_start_to_end_using_range(1, 5, 1)

def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    li = []
    num = start
    while(num < end+1):
      li.append(num)
      num = num + step
    return li
print(integers_from_start_to_end_using_while(1, 5, 1))

def is_prime(num):
  if num > 1:
    for i in range(2, num):
        if (num % i) == 0:
            return False
    else:
        return True

def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    primes = []
    for num in range(10, 100):
      if is_prime(num): 
        primes.append(num)
    return primes
print(two_digit_primes())