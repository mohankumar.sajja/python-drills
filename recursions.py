import json
with open('html_dict.json') as json_data:
    d = json.load(json_data)

def html_dict_search(html_dict, selector):
    """
    Implement `id` and `class` selectors
    """
    if isinstance(html_dict, dict):
        for k, v in html_dict.items():
            if k == selector:
                yield v
            else:
                yield from html_dict_search(v, selector)
    elif isinstance(html_dict, list):
        for item in html_dict:
            yield from html_dict_search(item, selector)

print("Id List: ", list(html_dict_search(d, 'id')))
print("Class List: ", list(html_dict_search(d, 'class')))