def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    str_dict = {}
    for word in list(s.split(" ")):
      if word in str_dict:
        str_dict[word] += 1
      else:
        str_dict[word] = 1
    print(str_dict)

word_count("Hi this is Mohan hi this is Mohan how are you")


def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    return [(key, value) for key, value in d.items()]

print(dict_items({'a':1, 'b':2, 'c':3}))

def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    return sorted([(key, value) for key, value in d.items()])
print(dict_items_sorted({'c':1, 'b':2, 'a':3}))
