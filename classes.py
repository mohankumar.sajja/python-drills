"""
* Create a class called `Point` which has two instance variables,
`x` and `y` that represent the `x` and `y` co-ordinates respectively. 

* Initialize these instance variables in the `__init__` method

* Define a method, `distance` on `Point` which accepts another `Point` object as an argument and 
returns the distance between the two points.
"""

# def distance(obj):
#   return obj.x - obj.y

class Point:

    def __init__(self, x, y):
      self.x = x
      self.y = y

    def distance(self, obj):
      return obj.x - obj.y 

obj = Point(10, 3)
print(obj.distance(obj))
# print(distance(obj))