def unique(items):
    """
    Return a set of unique values belonging to the `items` list
    """
    return set(items)
print(unique([1, 2, 3, 3, 2, 1, 2]))

import random
def shuffle(items):
    """
    Shuffle all items in a list
    """
    random.shuffle(items)
    return items

print(shuffle([1, 2, 3, 4, 5]))

import os
def getcwd():
    """
    Get current working directory
    """
    print(os.getcwd())
getcwd()

def mkdir(name):
    """
    Create a directory at the current working directory
    """
    try:
      os.mkdir('/home/runner/'+name)
    except FileExistsError:
      print('Hello your file is already is there!')

mkdir('mohan1.py')